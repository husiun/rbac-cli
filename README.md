# rbac-cli

权限脚手架系统

![输入图片说明](https://images.gitee.com/uploads/images/2018/0815/212234_c91546c3_1152471.png "屏幕截图.png")

一款基于Spring+SpringMVC+Mybatis+Shiro的一个权限脚手架，用户可在此基础上很方便的进行二次开发！

# 技术特色

- Spring
- SpringMVC
- Mybatis
- Pagehelper
- Druid
- Shiro
- Kaptcha
- Lombok
- Logback
- Freemarker
- commons工具包
- Guava

# 界面预览

![输入图片说明](https://images.gitee.com/uploads/images/2018/0815/212437_ffb86fb5_1152471.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0815/212542_ce134d8f_1152471.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0815/212501_9def9ff2_1152471.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0815/212600_15b7f7fe_1152471.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0815/212516_584fe9df_1152471.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0815/212617_1dfb5b15_1152471.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0815/212848_e5d65b7f_1152471.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0815/212911_a4096f40_1152471.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0815/212922_dcb8463f_1152471.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0815/213158_13204ecb_1152471.png "屏幕截图.png")