package cn.mrx.eas;

import cn.mrx.eas.dao.sys.*;
import cn.mrx.eas.dto.sys.SysPermission;
import cn.mrx.eas.dto.sys.SysRole;
import cn.mrx.eas.dto.sys.SysRolePermission;
import cn.mrx.eas.service.sys.ISysRoleService;
import cn.mrx.eas.vo.sys.TreeNode;
import com.google.common.base.Splitter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Mr.X
 * Date: 2017/12/24 上午10:42
 * Description:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/applicationContext-service.xml", "classpath:spring/applicationContext-dao.xml"})
@Slf4j
public class MapperTest {


    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Autowired
    private SysPermissionMapper sysPermissionMapper;

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    @Autowired
    private SysRolePermissionMapper sysRolePermissionMapper;


    @Autowired
    private ISysRoleService iSysRoleService;

    @Test
    public void test() {
        System.out.println("---------------------");
        /*String[] permissionIdArr = "".split(",");
        for (String str : permissionIdArr) {
            System.out.println(str);
        }*/

        List<String> list = Splitter.on(",").splitToList("");
        System.out.println(CollectionUtils.isEmpty(list));
        for (String str : list) {
            System.out.println(str);
        }
        System.out.println("---------------------");
    }
}
