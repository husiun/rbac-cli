/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50720
 Source Host           : localhost
 Source Database       : db_rbac-cli

 Target Server Type    : MySQL
 Target Server Version : 50720
 File Encoding         : utf-8

 Date: 01/22/2018 13:46:34 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `sys_permission`
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `url` varchar(40) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `sys_permission`
-- ----------------------------
BEGIN;
INSERT INTO `sys_permission` VALUES ('1', '根目录', null, '0', '0', '2018-01-04 14:21:20'), ('2', '角色权限管理', 'ROLE_PERMISSION_MANAGER', '0', '1', '2017-12-24 10:15:51'), ('3', '角色管理', 'sys:role:list', '1', '2', '2017-12-24 10:17:09'), ('4', '新增', 'sys:role:add', '2', '3', '2017-12-24 10:20:25'), ('5', '修改', 'sys:role:edit', '2', '3', '2017-12-24 10:20:28'), ('6', '删除', 'sys:role:batchDel', '2', '3', '2017-12-24 15:49:40'), ('7', '权限管理', 'sys:permission:list', '1', '2', '2017-12-24 15:50:59'), ('8', '新增', 'sys:permission:add', '2', '7', '2017-12-27 18:02:44'), ('9', '修改', 'sys:permission:edit', '2', '7', '2017-12-28 16:23:20'), ('10', '删除', 'sys:permission:batchDel', '2', '7', '2017-12-28 16:24:24'), ('11', '用户管理', 'sys:user', '1', '2', '2017-12-28 20:36:52'), ('12', '新增', 'sys:user:add', '2', '11', '2017-12-29 15:55:43'), ('13', '修改', 'sys:user:edit', '2', '11', '2017-12-29 15:56:33'), ('14', '删除', 'sys:user:batchDel', '2', '11', '2017-12-29 16:07:38'), ('15', '系统管理', 'SYSTEM_MANAGER', '0', '1', '2017-12-29 16:07:42'), ('16', '数据源监控', 'druid', '1', '15', '2017-12-29 16:07:46'), ('17', '定时管理', 'busi:schedule:list', '1', '15', '2017-12-29 16:13:42'), ('18', '新增', 'busi:schedule:add', '2', '17', '2017-12-29 16:14:47'), ('19', '修改', 'xxx', '2', '17', '2018-01-04 14:18:12'), ('20', '删除', 'xxx', '2', '17', '2018-01-04 14:18:16'), ('40', '考题管理', '', '0', '1', '2018-01-04 14:23:59'), ('41', '我的成绩', 'xxx', '1', '40', '2018-01-04 14:25:36'), ('43', '404测试', '404Test', '1', '15', '2018-01-17 15:20:37'), ('44', '500测试', '500Test', '1', '15', '2018-01-17 15:21:19'), ('45', '权限节点', 'sys:role:allNodes', '2', '3', '2018-01-17 15:59:42'), ('46', '权限节点', 'sys:permission:allNodes', '2', '7', '2018-01-17 16:00:03'), ('47', '考生减压', 'STRESS_RELIEVER', '0', '1', '2018-01-18 14:08:00'), ('48', '山区', 'stress1', '1', '47', '2018-01-18 14:08:28'), ('49', '烤火', 'stress2', '1', '47', '2018-01-18 14:08:39'), ('50', '沙滩', 'stress3', '1', '47', '2018-01-18 14:08:52'), ('51', '下雨声', 'stress4', '1', '47', '2018-01-18 14:09:06'), ('52', '顶部菜单-个人信息', 'TOP_MENU-PERSONAL_INFORMATION', '0', '1', '2018-01-21 11:04:43'), ('53', '个人信息', 'sys:user:pi', '1', '52', '2018-01-21 11:06:19');
COMMIT;

-- ----------------------------
--  Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `remarks` varchar(30) DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `sys_role`
-- ----------------------------
BEGIN;
INSERT INTO `sys_role` VALUES ('1', 'WEB_MANAGER', '网站管理员：可以负责网站的基础配置/分配权限等', '1', '2017-12-24 10:10:10'), ('2', 'TEACHER', '教师', '1', '2017-12-24 10:10:13'), ('3', 'STUDENT', '学生', '1', '2018-01-17 14:03:41');
COMMIT;

-- ----------------------------
--  Table structure for `sys_role_permission`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `permission_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=917 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `sys_role_permission`
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_permission` VALUES ('867', '1', '1'), ('868', '1', '2'), ('869', '1', '3'), ('870', '1', '4'), ('871', '1', '5'), ('872', '1', '6'), ('873', '1', '45'), ('874', '1', '7'), ('875', '1', '8'), ('876', '1', '9'), ('877', '1', '10'), ('878', '1', '46'), ('879', '1', '11'), ('880', '1', '12'), ('881', '1', '13'), ('882', '1', '14'), ('883', '1', '15'), ('884', '1', '16'), ('885', '1', '17'), ('886', '1', '18'), ('887', '1', '19'), ('888', '1', '20'), ('889', '1', '43'), ('890', '1', '44'), ('891', '1', '40'), ('892', '1', '41'), ('893', '1', '47'), ('894', '1', '48'), ('895', '1', '49'), ('896', '1', '50'), ('897', '1', '51'), ('909', '3', '1'), ('910', '3', '40'), ('911', '3', '41'), ('912', '3', '47'), ('913', '3', '48'), ('914', '3', '49'), ('915', '3', '50'), ('916', '3', '51');
COMMIT;

-- ----------------------------
--  Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `really_name` varchar(4) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `mobile` varchar(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `login_ip` varchar(15) DEFAULT NULL,
  `login_times` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `sys_user`
-- ----------------------------
BEGIN;
INSERT INTO `sys_user` VALUES ('1', 'admin', '3f8b0a128e6695b34a3f4a024b59c595', 'Mr.X', 'admin@qq.com', '18227593834', '0', '2017-12-23 16:41:59', '2018-01-17 10:51:44', '172.0.0.1', '10'), ('2', 'zhangsan', '3f8b0a128e6695b34a3f4a024b59c595', '张三', 'zhangsan@qq.com', '13211111111', '0', '2017-12-23 16:42:51', '2018-01-21 10:41:26', '172.0.0.1', '10'), ('3', 'lisi', '3f8b0a128e6695b34a3f4a024b59c595', '李四', 'lisi@qq.com', '13222222222', '0', '2017-12-23 16:47:10', '2018-01-16 14:39:20', null, null), ('4', 'wangwu', '3f8b0a128e6695b34a3f4a024b59c595', '王五', 'wangsu@qq.com', '13233333333', '0', '2017-12-23 16:47:14', '2018-01-16 14:39:08', null, null), ('5', 'zhaoliu', '3f8b0a128e6695b34a3f4a024b59c595', '赵六', 'zhaoliu@qq.com', '13244444444', '0', '2017-12-23 16:47:19', '2018-01-16 14:38:57', null, null), ('6', 'sunqi', '3f8b0a128e6695b34a3f4a024b59c595', '孙七', 'sunqi@qq.com', '13255555555', '0', '2017-12-23 16:47:22', '2018-01-17 15:15:34', null, null), ('7', 'zhouba', '3f8b0a128e6695b34a3f4a024b59c595', '周八', 'zhouba@qq.com', '13266666666', '0', '2017-12-23 16:47:25', '2018-01-16 14:39:27', null, null), ('8', 'wujiu', '3f8b0a128e6695b34a3f4a024b59c595', '吴九', 'wujiu@qq.com', '13277777777', '1', '2017-12-23 16:47:28', '2018-01-17 15:15:13', null, null), ('9', 'zhenshi', '3f8b0a128e6695b34a3f4a024b59c595', '郑十', 'zhengshi@qq.com', '13288888888', '2', '2017-12-23 16:47:33', '2018-01-18 16:27:51', null, null);
COMMIT;

-- ----------------------------
--  Table structure for `sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `sys_user_role`
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_role` VALUES ('23', '5', '3'), ('25', '4', '3'), ('26', '3', '2'), ('27', '7', '3'), ('39', '13', '3'), ('43', '14', '3'), ('44', '15', '3'), ('46', '16', '3'), ('47', '16', '4'), ('50', '1', '1'), ('52', '8', '3'), ('53', '6', '3'), ('62', '9', '3'), ('63', '2', '1');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
