package cn.mrx.eas.service.sys;

import cn.mrx.eas.dto.sys.SysUser;
import cn.mrx.eas.server.BSGrid;
import cn.mrx.eas.server.ServerResponse;

/**
 * Author: Mr.X
 * Date: 2017/12/23 下午5:25
 * Description:
 */
public interface ISysUserService {

    BSGrid userListPage(Integer curPage, Integer pageSize);

    SysUser findByUsername(String username);

    SysUser findOneById(Integer id);

    ServerResponse editUser(SysUser sysUser, Integer[] roleIds);

    ServerResponse addUser(SysUser sysUser, Integer[] roleIds);

    ServerResponse batchDel(String ids);
}